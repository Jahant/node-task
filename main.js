const express = require("express");
const app = express();
const PORT = 3000;

// build small REST API with Express
console.log("Server-side program starting...");

// baseurl: http://localhost:3000
// Endpoint: http://localhost:3000

app.get('/', (req, res) => {
    res.send('Hello World');
});

/**
 * This arrow function adds two numbers together
 * @param {Number} a first number
 * @param {Number} b secon number
 * @returns {Number}
 */
const add = (a, b) => {
    const sum = a + b;
    return sum;
};

// Adding Endpoint: http://localhost:3000/add?a=value&b=value
app.get('/add', (req, res) => {
    console.log(req.query);
    const sum = add(parseFloat(req.query.a), parseFloat(req.query.b));
    res.send(sum.toString());
});

app.listen(PORT, () => console.log(
    `Server listening http://localhost:${PORT}`
));