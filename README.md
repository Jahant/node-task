# Hello World

Let's mark some things down!

## Let's start with a cat

![gentlecat](./Avatar.png)

*This is a picture of a gentlemanly cat.* 



The cat in the picture was called **Fatty**. He lived from 2007 to 2022.

He could:

1. Take a nap
2. Eat some food
3. Take a nap
4. ~~Once in a blue moon do some exercising~~

---
## Enough about cats, let's do some marking

*But I thought we were marking things down already.*

Is there someone narrating this file? 

*Umm.. It's just your brain reading things with different voice*

Oh.. Okay. Let's list what we are going to do next. 

- First we are going to do a **list**
- Then we might do a **line** or two
- Maybe a **quote**?
- What's left? Ahh, some **code** will do

---
---
> There is no spoon

`# This python program prints Hello, world!`

`print('Hello, world!')`

## THE END